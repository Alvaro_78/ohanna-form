import React, { Component } from 'react'
import './App.css'
import TodoForm from './components/TodoForm'
import request from 'superagent'



class App extends Component {
  
  constructor(){
    super()
    this.state = {
      todos: [],
      users: ''
    }
    this.handleAddToDo = this.handleAddToDo.bind(this)
    this.removeTodo = this.removeTodo.bind(this)
  }

  componentDidMount () {
    request
    .get('http://localhost3001/app/users')
    .end((err, res) => {
  
    })
  }

  handleAddToDo(todo){
    this.setState({
      todos: [...this.state.todos, todo]
    })
  }

removeTodo(index){
  if(window.confirm ('estas seguro de querer borrar la tarea'))
  this.setState({
    todos: this.state.todos.filter((e, i) => {
      return i !== index
    })
  })
}

render(){
  const todos = this.state.todos.map((todo, i) => {
    return(
        <div className="col.md-4" key={i}>
          <div className="card mt-4">
            <div className="card-header"> 
              <h3>{todo.title}</h3>
              <span className="badge badge-pill badge-danger ml-2">
                {todo.priority}
              </span>
            </div>
            <div className="card-body">
              <p>{todo.description}</p>
              <p>{todo.responsible}</p>
            </div>
            <div className="card-footer">
              <button
              className="btn btn-danger"
              onClick={this.removeTodo.bind(this, i)}
              >
                Delete
              </button>
            </div>
          </div>
        </div>
      )
    })  
    
    return (

      <div className="App">
        <nav className="navbar navbar-dark bg-dark">
          <a href="btn" className="text-blue">
            <h1>Crea una tarea</h1> 
          <span className="badge badge-pill">
           <p className="row mt-5">Número de tareas: {this.state.todos.length}</p>
          </span> 
          </a>
        </nav>
          <div className="container">
            <div className="row mt-5">
              <div className="col-md-4">
              <TodoForm  onAddToDo={this.handleAddToDo}/>
              </div>
              <div className="col-md-5">
                <div className="row">
                  { todos }
                </div>
              </div>
            </div>
          </div>
      </div>
    )
  }
}

export default App